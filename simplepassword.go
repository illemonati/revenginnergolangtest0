package main

import (
	"bufio"
	"fmt"
	"os"
)

const REALPASSWORD = "peepo"

func main() {
	fmt.Println("Enter password!")
	password, _, err := bufio.NewReader(os.Stdin).ReadLine()
	if err != nil {
		panic("Could not read password!")
	}
	if string(password) == REALPASSWORD {
		fmt.Println("You solved it!")
	} else {
		fmt.Println("You did not solve it!")
	}
}
